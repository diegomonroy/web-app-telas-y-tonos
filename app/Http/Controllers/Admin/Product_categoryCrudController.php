<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Product_categoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class Product_categoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Product_categoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
	use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Product_category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product_category');
        $this->crud->setEntityNameStrings(trans('general.product_category'), trans('general.product_categories'));

		/*
		|--------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		// Reorder
		$this->crud->set('reorder.label', 'name');
		$this->crud->set('reorder.max_level', 0);

		// Export Buttons
		$this->crud->enableExportButtons();

		/*
		|--------------------------------------------------------------------------
		| PERMISSIONS
		|--------------------------------------------------------------------------
		*/

// 		// Read
// 		if ( $user->hasPermissionTo( 'Ver Categorías de Producto' ) ) {
// 			$this->crud->allowAccess( 'list' );
// 		} else {
// 			$this->crud->denyAccess( 'list' );
// 		}

// 		// Create
// 		if ( $user->hasPermissionTo( 'Crear Categorías de Producto' ) ) {
// 			$this->crud->allowAccess( 'create' );
// 		} else {
// 			$this->crud->denyAccess( 'create' );
// 		}

// 		// Update
// 		if ( $user->hasPermissionTo( 'Editar Categorías de Producto' ) ) {
// 			$this->crud->allowAccess( 'update' );
// 		} else {
// 			$this->crud->denyAccess( 'update' );
// 		}

// 		// Delete
// 		if ( $user->hasPermissionTo( 'Borrar Categorías de Producto' ) ) {
// 			$this->crud->allowAccess( 'delete' );
// 		} else {
// 			$this->crud->denyAccess( 'delete' );
// 		}

	}

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* Categoría de Producto */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.product_category'),
		]);

		/* Padre */

		$this->crud->addColumn([
			'label' => trans('general.parent'),
			'type' => 'select',
			'name' => 'parent_id',
			'entity' => 'parent',
			'attribute' => 'name',
			'model' => 'App\Models\Product_category',
		]);

	}

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(Product_categoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------
		*/

		/* Categoría de Producto */

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.product_category'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Padre */

		$this->crud->addField([
			'label' => trans('general.parent'),
			'type' => 'select2',
			'name' => 'parent_id',
			'entity' => 'parent',
			'attribute' => 'name',
			'model' => 'App\Models\Product_category',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
