<!-- Begin Dashboard -->
	<li class="nav-item"><a href="{{ backpack_url('dashboard') }}" class="nav-link"><i class="fa fa-dashboard nav-icon"></i> <span>{{ trans('general.dashboard') }}</span></a></li>
<!-- End Dashboard -->
<!-- Begin Filters -->
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-filter nav-icon"></i> <span>{{ trans('general.filters') }}</span></a>
		<ul class="nav-dropdown-items">
			@can('Ver Categorías De Producto')
			<li class="nav-item"><a href="{{ backpack_url('product_category') }}" class="nav-link"><i class="fa fa-list nav-icon"></i> <span>{{ trans('general.product_categories') }}</span></a></li>
			@endcan
		</ul>
	</li>
<!-- End Filters -->
<!-- Begin Files -->
	@can('Ver Archivos')
	<li class="nav-item"><a href="{{ backpack_url('elfinder') }}" class="nav-link"><i class="fa fa-files-o nav-icon"></i> <span>{{ trans('general.files') }}</span></a></li>
	@endcan
<!-- End Files -->
<!-- Begin Users -->
	@can('Ver Usuarios')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-group nav-icon"></i> <span>{{ trans('general.users') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('user') }}" class="nav-link"><i class="fa fa-user nav-icon"></i> <span>{{ trans('general.users') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('role') }}" class="nav-link"><i class="fa fa-group nav-icon"></i> <span>{{ trans('general.roles') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('permission') }}" class="nav-link"><i class="fa fa-key nav-icon"></i> <span>{{ trans('general.permissions') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Users -->
<!-- Begin Configuration -->
	@can('Ver Configuración')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-cogs nav-icon"></i> <span>{{ trans('general.configuration') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('setting') }}" class="nav-link"><i class="fa fa-cog nav-icon"></i> <span>{{ trans('general.settings') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('backup') }}" class="nav-link"><i class="fa fa-hdd-o nav-icon"></i> <span>{{ trans('general.backups') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('log') }}" class="nav-link"><i class="fa fa-terminal nav-icon"></i> <span>{{ trans('general.logs') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Configuration -->