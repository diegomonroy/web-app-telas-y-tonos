<?php

return [
	'backups' => 'Backups',
	'configuration' => 'Configuración',
	'dashboard' => 'Panel',
	'files' => 'Archivos',
	'filters' => 'Filtros',
	'id' => 'ID',
	'logs' => 'Logs',
	'parent' => 'Padre',
	'permissions' => 'Permisos',
	'product_category' => 'Categoría de Producto',
	'product_categories' => 'Categorías de Producto',
	'roles' => 'Roles',
	'settings' => 'Ajustes',
	'users' => 'Usuarios',
];